import request from 'supertest';
import app from '../app';

const user = {
    username : 'PepitoSeeds',
    email : 'seeds@gmail.com',
    password: 'pepitoseeds'
}

describe('Register & Login User', function() {
    it('This register should work', function(done) {
        request(app)
            .post('/auth/register')
            .send(user)
            .expect(200, done());
    });

    it('This login should work', function(done) {
        request(app)
            .post('/auth/login')
            .send({
                email : user.email,
                password: user.password
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });

    it('This login should\'t work', function(done) {
        request(app)
            .post('/auth/login')
            .send({
                email : user.email,
                password: user.username
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
      });

});

/* describe('GET /users', function() {
    it('respond with json', function(done) {
      request(app)
        .get('/users/')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it('respond with json', function(done) {
        request(app)
          .get('/users/asd')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200, done);
      });

    it('respond with json', function(done) {
        request(app)
          .post('/users/')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200, done);
      });
}); */

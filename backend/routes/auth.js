import { Router } from "express";
import User from '../models/User';
import jwt from 'jsonwebtoken';
import passport from 'passport';

const router = Router();

router.post('/register', async (req, res) => {
    try{
        let { username, email, password } = req.body;
        let user = new User({username, email});

        User.register(user, password, (err, account) => {
            console.log(account);
            if(err) return res.status(500).json(err);
            passport.authenticate('local', {session : false})(req, res, (account) => {
                res.status(200).json({msg : 'Your account has registed. Just login now!', account});
            });
        });

    } catch(err){
        return res.status(500).json(err);
    }   
});

router.post('/login', async (req, res, next) => {
    try{

        let {email, password} = req.body;

        if(!email || !password) return res.status(400).json({msg : "Send data"});

        passport.authenticate('local', { session : false }, (err, user, info) => {
            if(err || !user) return res.status(400).json({msg : "Rip"});
            
            req.login(user, { session : false }, (err) => {
                if(err) return res.status(500).send(err);
                const token = jwt.sign({id : user.id, email : user.email}, 'secret',{
                    expiresIn : '1d'
                });
                user.last_login = Date.now();
                user.save();
                return res.status(200).json({message : `${req.user.username} has logged`, token});
            });

        })(req, res, next);

    } catch(err){
        return res.status(500).json(err);
    }
});

export default router;
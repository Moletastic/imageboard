import { Router } from "express";
import User from '../models/User';

const router = Router();

/* router.get('/', async (req, res) => {
    try {
        const users = await User.find();
        return res.status(200).json(users);
    } catch (err) {
        return res.status(500).json(err);
    }
}); */

router.get('/greet', (req, res) => {
    return res.status(200).json(req.user)
});

export default router;


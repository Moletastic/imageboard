import JWT from 'passport-jwt';
import Local from 'passport-local';
import User from '../models/User';

export default (passport) => {

    passport.use(new Local.Strategy({
        usernameField : 'email',
        passwordField : 'password'
    }, User.authenticate()));

    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());

    const opts = {
        jwtFromRequest : JWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey : 'secret'
    };

    passport.use(new JWT.Strategy(opts, (payload, done) => {
        // payload = { id , sub, iat }
        User.findOne({id : payload.sub}, (err, user) => {
            if(err) return done(err,false);
            if(user) {return done(err, user);}
            else{return done(err,false);}
        });
    })); 
}
   


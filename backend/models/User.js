import {model, Schema} from 'mongoose';
import passportLocalMongoose from 'passport-local-mongoose';
import bcrypt from 'bcryptjs';

const userSchema = new Schema({
    username : {
        type : String,
        required : true
    },
    password : {
        type : String
    },
    email : {
        type : String,
        required : true
    },
    last_login : {
        type: Date
    },
    followers : [String],
    follows : [String]
});

/* userSchema.pre('save', function(next) {
    let user = this;
    if(this.isModified('password') || this.isNew){
        bcrypt.genSalt(10, function(err, salt){
            if(err) return next(err);
            bcrypt.hash(user.password, salt, function(err, hash){
                if(err) return next(err);
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

userSchema.methods.comparePassword = function(pass, cb){
    bcrypt.compare(pass, this.password, function(err, isMatch){
        if(err) cb(err);
        cb(null, isMatch);
    })
}; */

userSchema.methods.follow = function(followed){
    if(!this.follows.includes(followed._id.toString())){
        this.follows.push(followed._id);
        followed.followers.push(this._id);
        return true;
    } else {
        return false
    }
};

userSchema.methods.unfollow = function(followed){
    const id = followed._id.toString()
    if(!this.follows.includes(id)){
        return false;
    } else {
        this.follows.splice(this.follows.indexOf(id), 1);
        followed.followers.splice(this._id.toString(),1);
        return true;
    }
};

userSchema.plugin(passportLocalMongoose,{usernameField : 'email'});

const userModel = model('User',userSchema,'users');

export default userModel;
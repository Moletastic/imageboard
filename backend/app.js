"use strict";

import express from 'express';
import logger from 'morgan';
import helmet from 'helmet';
import cors from 'cors';
import mongoose from 'mongoose';
import dbconfig from './config/database';
import passport from 'passport';
import passportConf from './config/passport';

const app = express();
mongoose.connect(dbconfig.local.uri, {useNewUrlParser : true} )
    .then( db => {
        console.log("MongoDB is connected!");
    })
    .catch(err => {
        console.log(err);
    })

app
    .use(logger('dev'))
    .use(helmet())
    .use(cors())
    .use(express.urlencoded({extended : true}))
    .use(express.json())
    .use(passport.initialize());

passportConf(passport);


import router from './routes/users';
import authRouter from './routes/auth';

app.use('/users', passport.authenticate('jwt', {session:false}) ,router);
app.use('/auth',authRouter);

/* app.listen(3000, () => {
    console.log('Server running ...');
}) */

module.exports = app;